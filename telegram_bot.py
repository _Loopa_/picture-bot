import telebot
from picture import search
from telebot import types


Token = '1963291711:AAEGivXgDPjyd2dZwp3Qbc7llHpxWOjNqzg'
bot = telebot.TeleBot(Token)

code = ''
'''code_1 = {}
code_2 = {}'''


@bot.message_handler(content_types=['text'])
def start(message):
    if message.text == '/start':
        bot.send_message(message.from_user.id, "Hello. Input text to find a picture")
        bot.register_next_step_handler(message, get_message)
    else:
        bot.send_message(message.from_user.id, 'Call /start')


def get_message(message):
    global code
    # global code, code_1, code_2
    try:
        code = message.text
        search(code)
        img = open('docker_image_3.png', 'rb')
        bot.send_photo(message.chat.id, img)
        bot.register_next_step_handler(message, get_message)
    except TypeError:
        bot.send_message(message.from_user.id, "incorrect argument, input again")
        bot.register_next_step_handler(message, get_message)


print("The Telegram-script is running")
bot.polling(none_stop=True)