from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def search(text):
    options = webdriver.ChromeOptions()
    capabilities = options.to_capabilities()
    driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', desired_capabilities=capabilities)
    driver.get("http://google.com")
    driver.implicitly_wait(5)
    driver.save_screenshot('./Screenshots/docker_image_1.png')
    #elem = driver.find_element_by_xpath('/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')
    elem = driver.find_element_by_css_selector('.input')
    elem.send_keys(text)
    elem.send_keys(Keys.RETURN)
    driver.implicitly_wait(5)
    driver.save_screenshot('./Screenshots/docker_image_2.png')
    driver.implicitly_wait(5)
    #driver.find_element_by_xpath('/html/body/div[7]/div/div[3]/div/div[1]/div/div[1]/div/div[2]/a').click()
    driver.find_element_by_xpath('.nth-child(2)').click()
    driver.implicitly_wait(5)
    driver.save_screenshot('./Screenshots/docker_image_3.png')
    driver.quit()
    print('pictures created')
